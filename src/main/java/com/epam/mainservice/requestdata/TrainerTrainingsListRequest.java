package com.epam.mainservice.requestdata;

import java.time.LocalDate;

import lombok.Data;

@Data
public class TrainerTrainingsListRequest {
	private LocalDate from;
	private LocalDate to;
	private String traineeName;
}
