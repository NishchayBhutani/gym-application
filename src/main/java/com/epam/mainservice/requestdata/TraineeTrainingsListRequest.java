package com.epam.mainservice.requestdata;

import java.time.LocalDate;

import lombok.Data;

@Data
public class TraineeTrainingsListRequest {
	private LocalDate from;
	private LocalDate to;
	private String trainerName;
	private String trainingType;
}
