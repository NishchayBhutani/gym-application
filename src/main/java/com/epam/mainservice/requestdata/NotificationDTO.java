package com.epam.mainservice.requestdata;

import java.util.Map;

import lombok.Data;

//@Data
//@Builder
//@NoArgsConstructor
//@AllArgsConstructor
//public class NotificationDTO {
//	private String id;
//	private String from;
//	private String to;
//	private String cc;
//	private String subject;
//	private String body;
//	private String status;
//	private String remarks;
//}
@Data
public class NotificationDTO {
	private String to;
	private String cc;
	private String from;
	private Map<String, String> parameters;
	private String emailType;
}
