package com.epam.mainservice.requestdata;

import java.util.List;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class TrainerEmailListRequest {
	@NotBlank
	private List<String> trainerEmailList;
}
