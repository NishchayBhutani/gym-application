package com.epam.mainservice.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.mainservice.dao.User;

public interface UserRepository extends JpaRepository<User, Integer>{
	public boolean existsByEmailAndPassword(String email, String password);
	public Optional<User> findByEmailAndPassword(String email, String password);
}
