package com.epam.mainservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.mainservice.dao.Training;

public interface TrainingRepository extends JpaRepository<Training, Integer>{

}
