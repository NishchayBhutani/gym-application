package com.epam.mainservice.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.mainservice.dao.Trainer;

public interface TrainerRepository extends JpaRepository<Trainer, Integer>{
	public Optional<Trainer> findByUserEmail(String email);
}
