package com.epam.mainservice.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.mainservice.dao.Trainee;

public interface TraineeRepository extends JpaRepository<Trainee, Integer> {
	public Optional<Trainee> findByUserEmail(String email);

	public void deleteByUserEmail(String email);
}
