package com.epam.mainservice.utility;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;

import com.epam.mainservice.dao.Training;
import com.epam.mainservice.requestdata.NotificationDTO;
import com.epam.mainservice.responsedata.TraineeProfileResponse;
import com.epam.mainservice.responsedata.TrainerProfileResponse;
import com.epam.mainservice.responsedata.UserCreationResponse;

public class MessageMapper {

	private MessageMapper() {
	}

	@Value("${app.notification.from}")
	private static String notificationFrom = "gymapp@gmail.com";

	public static NotificationDTO getNotificationDTO(UserCreationResponse userCreationResponse) {
		NotificationDTO notificationDTO = new NotificationDTO();
		notificationDTO.setFrom(notificationFrom);
		notificationDTO.setTo(userCreationResponse.getEmail());
		notificationDTO.setEmailType("REGISTRATION");
		Map<String, String> parameters = new LinkedHashMap<>();
		parameters.put("email", userCreationResponse.getEmail());
		parameters.put("password", userCreationResponse.getPassword());
		notificationDTO.setParameters(parameters);
		return notificationDTO;
	}

	public static NotificationDTO getNotificationDTO(TraineeProfileResponse traineeProfileResponse) {
		NotificationDTO notificationDTO = new NotificationDTO();
		notificationDTO.setFrom(notificationFrom);
		notificationDTO.setTo(traineeProfileResponse.getEmail());
		notificationDTO.setEmailType("TRAINEE_UPDATION");
		Map<String, String> parameters = new LinkedHashMap<>();
		parameters.put("firstName", traineeProfileResponse.getFirstName());
		parameters.put("lastName", traineeProfileResponse.getLastName());
		parameters.put("dob", traineeProfileResponse.getDob().toString());
		parameters.put("address", traineeProfileResponse.getAddress());
		notificationDTO.setParameters(parameters);
		return notificationDTO;
	}

	public static NotificationDTO getNotificationDTO(TrainerProfileResponse trainerProfileResponse) {
		NotificationDTO notificationDTO = new NotificationDTO();
		notificationDTO.setFrom(notificationFrom);
		notificationDTO.setTo(trainerProfileResponse.getEmail());
		notificationDTO.setEmailType("TRAINER_UPDATION");
		Map<String, String> parameters = new LinkedHashMap<>();
		parameters.put("firstName", trainerProfileResponse.getFirstName());
		parameters.put("lastName", trainerProfileResponse.getLastName());
		parameters.put("training type", trainerProfileResponse.getTrainingType().getTrainingTypeName());
		notificationDTO.setParameters(parameters);
		return notificationDTO;
	}

	public static NotificationDTO getNotificationDTO(Training training) {
		NotificationDTO notificationDTO = new NotificationDTO();
		notificationDTO.setFrom(notificationFrom);
		notificationDTO.setTo(training.getTrainee().getUser().getEmail());
		notificationDTO.setEmailType("TRAINING_CREATION");
		notificationDTO.setCc(training.getTrainer().getUser().getEmail());
		Map<String, String> parameters = new LinkedHashMap<>();
		parameters.put("trainingName", training.getTrainingName());
		parameters.put("trainingDate", training.getTrainingDate().toString());
		parameters.put("trainingDuration", String.valueOf(training.getTrainingDuration()));
		parameters.put("traineeName",
				training.getTrainee().getUser().getFirstName() + " " + training.getTrainee().getUser().getLastName());
		parameters.put("traineeEmail", training.getTrainee().getUser().getEmail());
		parameters.put("trainerName",
				training.getTrainer().getUser().getFirstName() + " " + training.getTrainer().getUser().getLastName());
		parameters.put("trainerEmail", training.getTrainer().getUser().getEmail());
		notificationDTO.setParameters(parameters);
		return notificationDTO;
	}

}
