package com.epam.mainservice.responsedata;

import java.time.LocalDate;

import com.epam.mainservice.dao.TrainingType;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TrainerTrainingsListResponse {
	private String trainingName;
	private LocalDate trainingDate;
	private TrainingType trainingType;
	private int trainingDuration;
	private String traineeName;
}
