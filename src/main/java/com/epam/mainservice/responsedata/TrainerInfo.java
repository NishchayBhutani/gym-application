package com.epam.mainservice.responsedata;

import com.epam.mainservice.dao.TrainingType;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TrainerInfo {
	private String email;
    private String firstName;
    private String lastName;
    private TrainingType trainingType;
}
