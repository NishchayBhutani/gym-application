package com.epam.mainservice.responsedata;

import java.time.LocalDate;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TraineeProfileResponse {
	private String email;
	private String firstName;
	private String lastName;
	private LocalDate dob;
	private String address;
	private boolean isActive;
	private List<TrainerInfo> trainersList;
}
