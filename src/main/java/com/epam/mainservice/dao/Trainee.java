package com.epam.mainservice.dao;

import java.time.LocalDate;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data	
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Trainee {
	@Id
	@GeneratedValue
	private int id;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id")
	private User user;
	private LocalDate dob;
	private String address;
	@ManyToMany
	private List<Trainer> trainersList;
	@OneToMany
	private List<Training> trainingsList;
}
