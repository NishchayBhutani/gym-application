package com.epam.mainservice.dao;

import java.time.LocalDate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Training {
	@Id
	@GeneratedValue
	private int id;
	@ManyToOne
	@JoinColumn(name = "trainee_id")
	private Trainee trainee;
	@ManyToOne
	@JoinColumn(name = "trainer_id")
	private Trainer trainer;
	@Column(nullable = false)
	private String trainingName;
	@ManyToOne
	@JoinColumn(name = "training_type_id")
    private TrainingType trainingType;
	@Column(nullable = false)
	private LocalDate trainingDate;
	@Column(nullable = false)
	private int trainingDuration;
}