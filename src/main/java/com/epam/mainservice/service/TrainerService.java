package com.epam.mainservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.epam.mainservice.dao.Trainee;
import com.epam.mainservice.dao.Trainer;
import com.epam.mainservice.dao.Training;
import com.epam.mainservice.dao.TrainingType;
import com.epam.mainservice.dao.User;
import com.epam.mainservice.exception.TrainerException;
import com.epam.mainservice.kafka.NotificationProducer;
import com.epam.mainservice.repo.TrainerRepository;
import com.epam.mainservice.repo.TrainingTypeRepository;
import com.epam.mainservice.requestdata.TrainerCreationRequest;
import com.epam.mainservice.requestdata.TrainerTrainingsListRequest;
import com.epam.mainservice.requestdata.TrainerUpdateRequest;
import com.epam.mainservice.responsedata.TraineeInfo;
import com.epam.mainservice.responsedata.TrainerProfileResponse;
import com.epam.mainservice.responsedata.TrainerTrainingsListResponse;
import com.epam.mainservice.responsedata.UserCreationResponse;
import com.epam.mainservice.utility.MessageMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TrainerService {

	@Autowired
	TrainerRepository trainerRepository;

	@Autowired
	UserService userService;

	@Autowired
	TrainingTypeRepository trainingTypeRepository;

	@Autowired
	NotificationProducer notificationProducer;

	@Value("${app.notification.from}")
	private String notificationFrom;

	private static final String NOT_FOUND = "trainer not found";

	public UserCreationResponse register(TrainerCreationRequest trainerCreationRequest) {
		User user = User.builder().firstName(trainerCreationRequest.getFirstName())
				.lastName(trainerCreationRequest.getLastName()).email(trainerCreationRequest.getEmail())
				.password(UUID.randomUUID().toString()).isActive(true).build();
		userService.create(user);
		TrainingType trainingType = trainingTypeRepository
				.findByTrainingTypeName(trainerCreationRequest.getTrainingTypeName())
				.orElse(TrainingType.builder().trainingTypeName(trainerCreationRequest.getTrainingTypeName()).build());
		trainingTypeRepository.save(trainingType);
		Trainer trainer = Trainer.builder().user(user).trainingType(trainingType).build();
		trainerRepository.save(trainer);
		log.info("trainer created");
//		notificationProducer.sendNotification(NotificationDTO
//				.builder().from(notificationFrom).to(user.getEmail()).body("trainer registration successful!\nemail : "
//						+ user.getEmail() + "\npassword : " + user.getPassword())
//				.subject("registration success!").build());
		UserCreationResponse userCreationResponse = new UserCreationResponse(user.getEmail(), user.getPassword());
		notificationProducer.sendNotification(MessageMapper.getNotificationDTO(userCreationResponse));
		return userCreationResponse;
	}

	public TrainerProfileResponse getProfile(String email) {
		Trainer trainer = trainerRepository.findByUserEmail(email).orElseThrow(() -> new TrainerException(NOT_FOUND));
		List<TraineeInfo> traineeInfoList = new ArrayList<>();
		List<Trainee> traineeList = trainer.getTraineeList();
		for (Trainee trainee : traineeList) {
			traineeInfoList.add(new TraineeInfo(trainee.getUser().getEmail(), trainee.getUser().getFirstName(),
					trainee.getUser().getLastName()));
		}
		return new TrainerProfileResponse(email, trainer.getUser().getFirstName(), trainer.getUser().getLastName(),
				trainer.getTrainingType(), trainer.getUser().isActive(), traineeInfoList);
	}

	public TrainerProfileResponse updateProfile(String email, TrainerUpdateRequest trainerUpdateRequest) {
		Trainer trainer = trainerRepository.findByUserEmail(email).orElseThrow(() -> new TrainerException(NOT_FOUND));
		User user = trainer.getUser();
		user.setFirstName(trainerUpdateRequest.getFirstName());
		user.setLastName(trainerUpdateRequest.getLastName());
		user.setActive(trainerUpdateRequest.isActive());
		trainerRepository.save(trainer);
		log.info("trainer profile updated");
//		notificationProducer.sendNotification(NotificationDTO.builder().from(notificationFrom).to(user.getEmail())
//				.body("your trainer profile has been updated!").subject("profile updated").build());
		List<TraineeInfo> traineeInfoList = new ArrayList<>();
		List<Trainee> traineeList = trainer.getTraineeList();
		for (Trainee trainee : traineeList) {
			traineeInfoList.add(new TraineeInfo(trainee.getUser().getEmail(), trainee.getUser().getFirstName(),
					trainee.getUser().getLastName()));
		}
		TrainerProfileResponse trainerProfileResponse = new TrainerProfileResponse(email, trainer.getUser().getFirstName(), trainer.getUser().getLastName(),
				trainer.getTrainingType(), trainer.getUser().isActive(), traineeInfoList);
		notificationProducer.sendNotification(MessageMapper.getNotificationDTO(trainerProfileResponse));
		return trainerProfileResponse;
	}

	public List<TrainerTrainingsListResponse> getTrainingsList(String email,
			TrainerTrainingsListRequest trainerTrainingsListRequest) {
		List<Training> trainingsList = trainerRepository.findByUserEmail(email)
				.orElseThrow(() -> new TrainerException(NOT_FOUND)).getTrainingsList();
		List<TrainerTrainingsListResponse> trainerTrainingsResponseList = new ArrayList<>();
		List<Training> filteredTrainingsList = trainingsList.stream()
				.filter(training -> trainerTrainingsListRequest.getTraineeName() == null || training.getTrainer()
						.getUser().getFirstName().equals(trainerTrainingsListRequest.getTraineeName()))
				.filter(training -> trainerTrainingsListRequest.getFrom() == null
						|| training.getTrainingDate().compareTo(trainerTrainingsListRequest.getFrom()) >= 0)
				.filter(training -> trainerTrainingsListRequest.getTo() == null || training.getTrainingDate()
						.plusMonths(training.getTrainingDuration()).compareTo(trainerTrainingsListRequest.getTo()) <= 0)
				.toList();
		for (Training training : filteredTrainingsList) {
			trainerTrainingsResponseList.add(new TrainerTrainingsListResponse(training.getTrainingName(),
					training.getTrainingDate(), training.getTrainingType(), training.getTrainingDuration(),
					training.getTrainer().getUser().getFirstName()));
		}
		return trainerTrainingsResponseList;
	}
}
